debugShowCheckedModeBanner: false,

								form page:-


import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  

  @override
  _FormPageState createState() {
    return _FormPageState();
  }
}

class _FormPageState extends State<FormPage> {
 
  String _name,_email,_phone ;
  String colorGroupValue = '';
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();

final GlobalKey<FormState> _formkey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Form(

            key: _formkey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius:70 ,
                  child: Image.network("https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.kindpng.com%2Fpicc%2Fm%2F355-3557482_flutter-logo-png-transparent-png.png&imgrefurl=https%3A%2F%2Fwww.kindpng.com%2Fimgv%2FhihxibR_flutter-logo-png-transparent-png%2F&tbnid=xMG4hO4pG3-IEM&vet=12ahUKEwjB5c2xyfLuAhVETCsKHcNADcUQMygIegUIARDHAQ..i&docid=Vso70c5_zHFuFM&w=860&h=979&q=flutter%20logo&safe=active&ved=2ahUKEwjB5c2xyfLuAhVETCsKHcNADcUQMygIegUIARDHAQ"),
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15,left: 10, right: 10),
                  child: TextFormField(

                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      icon: Icon(Icons.person,color: Colors.indigo,),
                      labelText: 'Full Name',
                    ),
                    validator: (String value){
                      if(value.isEmpty)
                        {
                          return "please enter the value";
                        }
                      return null;
                    },
                    onSaved: (String name){
                      _name = name;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15,left: 10, right: 10),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      icon: Icon(Icons.email, color: Colors.indigo,),
                      labelText: 'Email',),
                    validator: (String value){
                      if(!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]").hasMatch(value))
                      {
                        return "please enter valid mail";
                      }
                      return null;
                    },
                    onSaved: (String email){
                      _email = email;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15,left: 10, right: 10),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      icon: Icon(Icons.contact_phone,color: Colors.indigo,),
                      labelText: 'Phone No',),
                    validator: (String value){
                      if(value.length < 9)
                      {
                        return "please enter 10 digit phone no";
                      }
                      return null;
                    },
                    onSaved: (String phone){
                      _phone = phone;
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(bottom: 15,left: 10, right: 10),
                  child: TextFormField(
                    controller: _password ,
                    obscureText: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock,color: Colors.indigo,),
                      labelText: 'Password',),
                    validator: (String value){
                      if(value.isEmpty)
                      {
                        return "please enter password";
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15,left: 10, right: 10),
                  child: TextFormField(
                    controller: _confirmPassword,
                    obscureText: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock,color: Colors.indigo,),
                      labelText: 'Confirm Password',),
                    validator: (String value){
                      if(value.isEmpty)
                      {
                        return "please re-enter password";
                      }
                      if(_password.text != _confirmPassword.text)
                        {
                          return "password is not matched";
                        }
                      return null;
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Radio(value: 'Male', groupValue: colorGroupValue,
                        onChanged: (val){
                          colorGroupValue =val;
                          setState(() {

                          });
                    }),
                    Text("Male"),
                    Radio(value: 'Female', groupValue: colorGroupValue,
                        onChanged: (val){
                          colorGroupValue = val;
                          setState(() {

                          });
                        }),
                    Text("Female"),
                  ],
                ),

                SizedBox(
                  width: 200,
                  height: 50,
                  child: RaisedButton(
                    color: Colors.redAccent,
                    onPressed: (){
                      if(_formkey.currentState.validate())
                        {
                          return;
                        }else
                          {
                            print("unsuccessfull");
                          }

                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0),
                      side: BorderSide(color: Colors.blue,width: 2),
                    ),
                    textColor: Colors.white,child: Text('Submit'),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:flutter_app/FormPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter app',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: FormPage(),
    );
    return null;
  }
}







import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {


  @override
  _FormPageState createState() {
    return _FormPageState();
  }
}

class _FormPageState extends State<FormPage> {

  String _name,_email,_phone ;
  String colorGroupValue = '';
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Form(

            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  color: Colors.blue,
                constraints: BoxConstraints.expand(
                height: Theme.of(context).textTheme.headline4.fontSize * 1.1 + 300.0,
                 ),
                child: Padding(
                    padding: EdgeInsets.only(left: 30, right:30, top: 30 ),
                    child: Column(
                      children: [
                        Row(
                          children:[
                            Container(
                              height: 50,
                              width: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,

                              ),
                            )

                          ],
                          )

                      ],
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(bottom: 15,left: 10, right: 10),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(

                      labelText: 'Email',),
                    validator: (String value){
                      if(!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]").hasMatch(value))
                      {
                        return "please enter valid mail";
                      }
                      return null;
                    },
                    onSaved: (String email){
                      _email = email;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15,left: 10, right: 10),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(

                      labelText: 'Phone No',),
                    validator: (String value){
                      if(value.length < 9)
                      {
                        return "please enter 10 digit phone no";
                      }
                      return null;
                    },
                    onSaved: (String phone){
                      _phone = phone;
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(bottom: 15,left: 10, right: 10),
                  child: TextFormField(
                    controller: _password ,
                    obscureText: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(

                      labelText: 'Twitter',),
                    validator: (String value){
                      if(value.isEmpty)
                      {
                        return "please enter password";
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15,left: 10, right: 10),
                  child: TextFormField(
                    controller: _confirmPassword,
                    obscureText: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(

                      labelText: 'FaceBook',),
                    validator: (String value){
                      if(value.isEmpty)
                      {
                        return "please re-enter password";
                      }
                      if(_password.text != _confirmPassword.text)
                      {
                        return "password is not matched";
                      }
                      return null;
                    },
                  ),
                ),



              ],
            ),
          ),
        ),
      ),
    );
  }
}




import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {


  @override
  _FormPageState createState() {
    return _FormPageState();
  }
}

class _FormPageState extends State<FormPage> {

  String _name, _email, _phone;

  String colorGroupValue = '';
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Form(

            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  color: Colors.yellow,
                  constraints: BoxConstraints.expand(
                    height: Theme
                        .of(context)
                        .textTheme
                        .headline4
                        .fontSize * 1.1 + 200.0,
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(left: 30, right: 30, top: 30),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(

                                shape: BoxShape.circle,
                                color: Colors.white,

                              ),
                            ),
                          ],

                        ),

                      ],

                    ),

                  ),

                ),


                Padding(
                  padding: const EdgeInsets.only(
                      bottom: 15, left: 10, right: 10),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(

                      labelText: 'Email',),
                    validator: (String value) {
                      if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                          .hasMatch(value)) {
                        return "please enter valid mail";
                      }
                      return null;
                    },
                    onSaved: (String email) {
                      _email = email;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      bottom: 15, left: 10, right: 10),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(

                      labelText: 'Phone No',),
                    validator: (String value) {
                      if (value.length < 9) {
                        return "please enter 10 digit phone no";
                      }
                      return null;
                    },
                    onSaved: (String phone) {
                      _phone = phone;
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(
                      bottom: 15, left: 10, right: 10),
                  child: TextFormField(
                    controller: _password,
                    obscureText: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(

                      labelText: 'Twitter',),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return "please enter password";
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      bottom: 15, left: 10, right: 10),
                  child: TextFormField(
                    controller: _confirmPassword,
                    obscureText: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(

                      labelText: 'FaceBook',),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return "please re-enter password";
                      }
                      if (_password.text != _confirmPassword.text) {
                        return "password is not matched";
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(
                  width: 200,
                  height: 50,
                  child: RaisedButton(
                    color: Colors.yellow,
                    onPressed: (){


                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0),
                      side: BorderSide(color: Colors.blue,width: 2),
                    ),
                    textColor: Colors.white,child: Text('update'),
                  ),
                )


              ],
            ),
          ),
        ),
      ),
    );
  }
}






						taimur Sikander:--

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    AssetImage joey = AssetImage('images/joey.jpg');
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      children: [
        Container(
            child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20, top: 30),
                child: Column(
                  children: [
                    Container(
                      child: GestureDetector(
                        onTap: () {},
                        child: Container(
                            width: 450,
                            height: 240,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                              colors: [Colors.amber[400], Colors.amber[400]],
                              begin: Alignment.topLeft,
                              end: Alignment.topRight,
                            )),
                            child: Container(
                                child: Row(
                              children: [
                                CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      'https://www.woolha.com/media/2020/03/eevee.png'),
                                  backgroundColor: Colors.red,
                                  radius: 40,
                                ),
                                Text("TAIMOOR SIKANDER",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold))
                              ],
                            ))),
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: Padding(
                            padding: EdgeInsets.all(0.0),
                            child: Icon(
                              Icons.person,
                            ), // icon is 48px widget.
                          ),
                          labelText: 'Full Name'),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        prefixIcon: Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Icon(
                            Icons.send,
                          ), // icon is 48px widget.
                        ),
                        labelText: 'EMAIL',
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        prefixIcon: Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Icon(
                            Icons.add_ic_call_outlined,
                          ), // icon is 48px widget.
                        ),
                        labelText: 'Phone Number',
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        prefixIcon: Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Icon(
                            Icons.lock_outline_rounded,
                          ), // icon is 48px widget.
                        ),
                        labelText: 'Password',
                      ),
                    ),
                    RaisedButton(
                      child: Text(
                        "Update",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      onPressed: () {
                        print('Button is Pressed');
                      },
                      color: Colors.amber[400],
                      textColor: Colors.black,
                      padding: EdgeInsets.only(
                          left: 152, right: 152, top: 10, bottom: 10),
                      splashColor: Colors.grey,
                    )
                  ],
                ))),
      ],
    )));
  }
}






					 Card(
            child: Container(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [Text("100"), SizedBox(height: 5,),Text("Documents")],
                  ),
                  Column(
                    children: [Text("50"), SizedBox(height: 5,),Text("Photos")],
                  ),
                  Column(
                    children: [Text("60"), SizedBox(height: 5,),Text("Videos")],
                  ),
                ],
              ),
            ),
          ),







Border.all(
                                              color: Color(0xFFE6E6E6)),
                                          color: Color(0xFFE6E6E6),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8.0)),
                                        )
karan Paul11:00 AM
 border: InputBorder.none,
                       
                              focusedBorder: InputBorder.none,







user management system 

 on profile show 





import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:restroapp/src/database/SharedPrefs.dart';
import 'package:restroapp/src/princeLLC/AppHelper/LanguageUtil.dart';

class ProductList extends StatefulWidget {
  ProductList({Key key}) : super(key: key);

  @override
  _ProductListState createState() {
    return _ProductListState();
  }
}

class _ProductListState extends State<ProductList> {

  bool showvalue = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Directionality(
        textDirection:
        LanguageUtil.isRTL() ? TextDirection.rtl : TextDirection.ltr,
    child: Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_sharp,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        title: Text('Your Cart',style: TextStyle(color: Colors.black),),
        backgroundColor: Colors.white,
      ),
      body: Column(
        children: [
          Expanded(
            flex: 5,

            child: Column(
            children: [
              Container(
                margin: EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('4 items',style: TextStyle(color: Colors.grey,fontSize: 12),),
                    RichText(
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: 'Add more products',
                            style: TextStyle(color: Colors.blue,fontSize: 12,fontWeight: FontWeight.bold),
                            recognizer: TapGestureRecognizer()..onTap = () {}),
                      ]),
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(

                  margin: EdgeInsets.only(left: 20,right: 20),
                  //height: 350,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white  ),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      children: [
                        ListView.builder(

                          itemCount: 3,
                          shrinkWrap: true,
                          itemBuilder:  (BuildContext context, int index){
                            return itemList();
                          },
                        ),
                        Divider(height: 5,thickness: 1,),
                        TextFormField(
                          decoration: InputDecoration(
                            prefixIcon: Image(
                              image: AssetImage('images/instruction_icon.png'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),),
          Flexible(
            flex: 3,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  padding: EdgeInsets.only(left: 30,right: 20,top: 30,),
                  width: double.infinity,
                  height:MediaQuery.of(context).size.height ,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(50)),
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Image(image: AssetImage('images/loyalty_points.png'),),
                          SizedBox(width: 10,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Loyalty Point'),
                              Text('\u0024220',style: TextStyle(color: Colors.blue),)
                            ],
                          ),
                          SizedBox(width: 160,),
                          Checkbox(

                            value: this.showvalue,
                            onChanged: (bool value) {
                              setState(() {
                                this.showvalue = value;
                              });
                            },
                          ),
                        ],
                      ),
                      Divider(height: 5,thickness: 1,),
                      SizedBox(height: 15,),
                      Row(
                        children: [
                          Image(image: AssetImage('images/promo_icon.png'),),
                          SizedBox(width: 15,),
                          Text('Select a promo Code'),
                          SizedBox(width: 60,),
                          RichText(
                            text: TextSpan(children: <TextSpan>[
                              TextSpan(
                                  text: 'View Coupon',
                                  style: TextStyle(color: Colors.blue,fontSize: 14,fontWeight: FontWeight.bold),
                                  recognizer: TapGestureRecognizer()..onTap = () {}),
                            ]),
                          ),

                        ],
                      ),
                      SizedBox(height: 15,),
                      Divider(height: 5,thickness: 1,),

                    ],
                  ),
                ),
              ),),



        ],
      ),
    ),);
  }

  Widget itemList() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        margin: EdgeInsets.only(top: 10,right: 5,bottom: 5),
          child: ListTile(
           leading: Image(
             image: AssetImage('images/placeHolder.png'),
             height: 70,
             width: 70,
           ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Fresh Milk (Blue)',style: TextStyle(fontWeight: FontWeight.bold),),
                    Text('1.5 Litter/30 Units',style: TextStyle(fontSize: 12),),
                    SizedBox(height: 4,),
                    Text('\u00242.00',style: TextStyle(fontSize: 11),),
                    Text('\u00242.50',style: TextStyle(fontSize: 11,decoration: TextDecoration.lineThrough),),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(height:20,width:20,child: IconButton(icon: Icon(Icons.delete_outlined),iconSize: 20,onPressed: (){},)),
                    Row(
                        children: [
                          IconButton(icon: Icon(Icons.add),color:Colors.blue,iconSize: 20,onPressed: (){},),
                          Container(height:20,width:20,child: Text('5')),
                          IconButton(icon: Icon(Icons.remove),color:Colors.blue,iconSize: 20,onPressed: (){},),

                        ],
                      ),
                  ],
                ),

              ],
            ),
          ),
      ),
    );
  }
}





 android:icon="@mipmap/ic_launcher">
        <meta-data android:name="com.google.android.geo.API_KEY"
            android:value="AIzaSyDfAa677KN510Kj1Xpub6ytht-C5IAJPoc"/>